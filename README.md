# Beanie
This repository contains the source code for the personal site.

## Starting Up
### Development
The first step that should be done is to ensure that the project is initially buildable and runnable.
1. Install the dependencies using *yarn*.
```sh
> yarn
```
2. Set the environment variables
```sh
> cp public/__/firebase/init.sample.json public/__/firebase/init.json
> vim public/__/firebase/init.json
# Insert neccessary info and save.
```
3. Run the development server.
```sh
> yarn start
```
4. Verify that the page is up and running at [localhost:3000](localhost:3000).

### Building for Production
Production build can be achieved using the following command:
```sh
> yarn build
```
Output files of the successful build will be located at */build* of the project. 

### Linting
ESLint is used in this repository:
```sh
> yarn lint
```

## Deployment
### Google App Engine
Deployment of the static site can be achieved through the use of google API however a couple of the steps have to be taken before deployment. 
Ensure that *gcloud cli* is installed and initialised before proceeding. Refer to [GCloud documentation](https://cloud.google.com/sdk/docs/).
A project should already be created in *Google Cloud Platform*.
1. Create a new folder that will house the deployment files.
```sh
> mkdir {directory-name}
```
2. Copy the *app.yaml* file to the deployment folder.
```sh
> cd {directory-name}
> cp {pathto-project-folder}/app.yaml .
```
3. Copy the build files to *www/* folder.
```sh
> mkdir www
> cp {pathto-project-folder}/build/* ./www/
```
4. Deploy to the project.
```sh
> gcloud app deploy --project {project-id}
```
The project is now successfully deployed.
