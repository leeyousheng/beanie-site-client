import React from 'react';
import ReactDOM from 'react-dom';
import 'typeface-roboto';
import firebase from 'firebase/app';

// Project Imports
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

fetch('/__/firebase/init.json').then(response => firebase.initializeApp(response.json()));

ReactDOM.render(
  <App />,
  document.getElementById('root'),
);
registerServiceWorker();
