import React from 'react';
import {
  Event,
  Photo,
  Home,
  LocalLibrary,
} from '@material-ui/icons';

const config = {
  items: {
    home: {
      home: {
        label: 'Home',
        icon: <Home />,
        url: '/',
      },
    },
    primary: {
      diary: {
        label: 'Diary',
        icon: <LocalLibrary />,
        url: 'diary',
      },
      events: {
        label: 'Events',
        icon: <Event />,
        url: 'events',
      },
      gallery: {
        label: 'Gallery',
        icon: <Photo />,
        url: 'gallery',
      },
    },
  },
};

export default config;
