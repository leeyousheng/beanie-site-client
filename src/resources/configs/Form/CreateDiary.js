// Project Imports
import { DateUtil } from '../../../utils';

const formInputs = [{
  label: 'Entry Date',
  name: 'entryDate',
  type: 'date',
  defaultValue: DateUtil.getInputDate(),
  flags: {
    required: true,
  },
}, {
  label: 'Title',
  name: 'title',
  type: 'text',
  flags: {
    required: true,
  },
},
{
  label: 'Diary Entry',
  name: 'text',
  type: 'text',
  flags: {
    rows: 4,
    multiline: true,
    required: true,
  },
}];

const gqlQuery = `
  mutation createDiary($input: DiaryInput!) {
    createDiary(input: $input) {
      id
      title
      text
      tags {
        id
        text
      }
    }
  }
`;

export default { formInputs, gqlQuery };
