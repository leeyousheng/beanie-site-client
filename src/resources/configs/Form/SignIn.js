const formInputs = [{
  label: 'Email',
  name: 'email',
  type: 'email',
  flags: {
    required: true,
  },
}, {
  label: 'Password',
  name: 'password',
  type: 'password',
  flags: {
    required: true,
  },
}];

export default { formInputs };
