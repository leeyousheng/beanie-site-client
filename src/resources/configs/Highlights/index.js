import Events from '../Events';

const Highlights = {
  type: 'event',
  ...Events.event2018091100,
};

export default Highlights;
