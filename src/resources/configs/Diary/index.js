const entries = {
  20180912: {
    title: 'I am so ACTIVE',
    createdOn: 'Wed Sep 12 2018 18:52:39 GMT+0800',
    text: `
      Today I am so active. I like to just kick around Mummy tummy. So fun.\n
      I think I want to be an active kid in the future and run around the whole day.\n
    `,
    tags: {
      aboutme: {
        text: 'About Me',
        type: 'info',
      },
    },
  },
  20180911: {
    title: 'My First Post',
    createdOn: 'Wed Sep 12 2018 14:52:39 GMT+0800',
    text: `
      Hi, My name currently is Beanie, spelt as B-E-A-N-I-E. Mummy tells me that it is only a temporary name and that I will be getting a new name soon, so excited!\n
      Hope my future name is very powerful so that I will have a blessed life.\n
      Currently I am residing in Mummy's tummy. It is so warm in here... soon I will be out on Oct 07, 2018! Wait for my arrival! Cya!\n
    `,
    tags: {
      aboutme: {
        text: 'About Me',
        type: 'info',
      },
      beanie: {
        text: 'Beaniieeee',
        type: 'event',
      },
    },
  },
};

export default entries;
