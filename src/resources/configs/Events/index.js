const Events = [{
  id: '1',
  title: 'Day of Birth',
  sub: 'something',
  eventDetails: {
    location: {
      icon: 'LocationOn',
      text: 'Mt Alvernia',
      onClick: () => console.log('Mt Alvernia clicked'),
    },
    time: {
      icon: 'AccessAlarm',
      text: 'TBA',
    },
  },
  description: 'Join us to withness the birth of our baby boy. May he have a bright future ahead!',
  options: {
    ok: {
      text: 'Count Me In',
      type: 'primary',
    },
    cmi: {
      text: 'Not this time',
      type: 'secondary',
    },
    notsure: {
      text: 'Dunno',
      type: 'default',
    },
  },
  image: {
    src: 'http://shawnsherelle.com/wp-content/uploads/2015/08/8-mount-alvernia-hospital.jpg',
    alt: 'event',
  },
},
{
  id: '2',
  title: 'Shower time',
  eventDetails: {
    location: {
      icon: 'LocationOn',
      text: '???',
    },
    time: {
      icon: 'AccessAlarm',
      text: 'TBA',
    },
  },
  description: 'Join us seeing the baby boy first month!',
  options: {
    ok: {
      text: 'Count Me In',
      type: 'primary',
    },
    cmi: {
      text: 'Not this time',
      type: 'secondary',
    },
  },
  image: {
    src: 'http://shawnsherelle.com/wp-content/uploads/2015/08/8-mount-alvernia-hospital.jpg',
    alt: 'event',
  },
}];

export default Events;
