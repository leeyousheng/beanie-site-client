const color = {
  event: {
    backgroundColor: '#ede7f6',
    color: 'black',
  },
  diary: {
    backgroundColor: '#fff3e0',
    color: 'black',
  },
  tag: {
    default: {
      backgroundColor: '#757575',
      color: 'white',
    },
    info: {
      backgroundColor: '#00838f',
      color: 'white',
    },
    event: {
      backgroundColor: '#43a047',
      color: 'black',
    },
  },
};

export default color;
