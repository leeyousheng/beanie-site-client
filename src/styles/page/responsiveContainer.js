const responsiveContainer = theme => ({
  width: 'auto',
  [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
    marginTop: theme.spacing.unit * 3,
    width: 1100,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
});

export default responsiveContainer;
