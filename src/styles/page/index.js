import responsiveContainer from './responsiveContainer';

const page = {
  responsiveContainer,
};

export default page;
