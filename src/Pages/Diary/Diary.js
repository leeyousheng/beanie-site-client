import React from 'react';
import {
  string,
  shape,
} from 'prop-types';
import {
  withStyles,
  Grid,
  Typography,
} from '@material-ui/core';

// Project Imports
import {
  Diary as DiaryComp,
  List,
  Query,
} from '../../components';
import {
  page as pageStyle,
} from '../../styles';

const styles = theme => ({
  page: {
    ...pageStyle.responsiveContainer(theme),
    padding: theme.spacing.unit,
  },
  text: {
    textAlign: 'center',
  },
});

const GET_DIARIES = `
  {
    allDiaries {
      id
      title
      text
      entryDate
      tags{
        id
        text
        type
      }
      author{
        displayname
        pic
      }
    }
  }
`;

const DiaryListItem = (props) => {
  const { item } = props;
  return (
    <Grid item xs={12}>
      <DiaryComp data={item} />
    </Grid>
  );
};

const MidList = props => <List {...props} component={DiaryListItem} />;

DiaryListItem.propTypes = {
  item: shape({}).isRequired,
};

const Diary = (props) => {
  const { classes } = props;

  return (
    <main className={classes.page}>
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <Typography variant="display1" className={classes.text}>
            Diary
          </Typography>
        </Grid>
        <Query
          query={GET_DIARIES}
          dataComponent={MidList}
        />
      </Grid>
    </main>
  );
};

Diary.propTypes = {
  classes: shape({
    page: string.isRequired,
    text: string.isRequired,
  }).isRequired,
};

export default withStyles(styles)(Diary);
