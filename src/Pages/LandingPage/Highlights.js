import React from 'react';
import {
  Grid,
} from '@material-ui/core';
import {
  shape,
} from 'prop-types';

// Project Imports
import HighlightItem from './HighlightItem';

const renderHighlightItem = (data) => {
  if (!data) return null;

  return (
    <Grid item xs={12}>
      <HighlightItem {...data} />
    </Grid>
  );
};

const Highlights = (props) => {
  const { data } = props;
  const { primary, secondary } = data;

  return (
    <Grid container spacing={16}>
      {renderHighlightItem(primary)}
      {renderHighlightItem(secondary)}
    </Grid>
  );
};

Highlights.propTypes = {
  data: shape({
    primary: shape({}),
    secondary: shape({}),
  }).isRequired,
};

export default Highlights;
