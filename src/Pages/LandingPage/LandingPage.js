import React from 'react';
import {
  shape,
  string,
} from 'prop-types';
import {
  withStyles,
  Grid,
} from '@material-ui/core';

// Project Imports
import { page as pageStyle } from '../../styles';
import {
  Text,
} from '../../components';

const styles = theme => ({
  page: {
    ...pageStyle.responsiveContainer(theme),
  },
  paper: {
    padding: theme.spacing.unit * 2,
  },
  profileText: {
    textAlign: 'center',
    verticalAlign: 'center',
  },
  img: {
    maxHeight: 300,
    maxWidth: '100%',
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: '100%',
  },
  maxWidth: {
    width: '100%',
  },
});
const profileText = 'Hi my name is Teyton Lee Jia Le\n李家乐.\nWelcome to my website and witness my growth!\nHope you have a great time here!';

const LandingPage = (props) => {
  const {
    classes,
  } = props;
  return (
    <main className={classes.page}>
      <Grid container spacing={16}>
        <Grid item md={6} xs={12}>
          <img className={classes.img} src="https://firebasestorage.googleapis.com/v0/b/eus-personal-sites-0001.appspot.com/o/public%2Fimages%2Fweb%2FBeanieProfilePic.jpeg?alt=media&token=ba3f55e8-0e09-463d-b3d4-88b56c7145af" alt="babyphoto" />
        </Grid>
        <Grid item md={6} xs={12}>
          <Text className={classes.profileText} text={profileText} variant="display1" />
        </Grid>
      </Grid>
    </main>
  );
};

LandingPage.propTypes = {
  classes: shape({
    page: string.isRequired,
  }).isRequired,
};

export default withStyles(styles)(LandingPage);
