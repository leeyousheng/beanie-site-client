import React from 'react';
import {
  string,
} from 'prop-types';

// Project Imports
import {
  Query,
  Event,
  Diary,
} from '../../components';

const GET_EVENT = `
query event($id:ID!){
  event(id: $id){
    id
    title
    eventDetails {
      name
      icon
      text
    }
    description
    options {
      name
      text
      type
    }
    image {
      src
      alt
    }
  }
}
`;

const GET_DIARY = `
query diary($id:ID!){
  diary(id: $id){
      id
      title
      text
      entryDate
      tags{
        id
        text
        type
      }
    }
  }
`;

const HighlightItem = (props) => {
  const { type, id } = props;

  let gqlQuery;
  let Component;
  switch (type) {
    case 'event':
      gqlQuery = GET_EVENT;
      Component = Event;
      break;
    case 'diary':
      gqlQuery = GET_DIARY;
      Component = Diary;
      break;
    default:
      throw new Error(`Unknown type: ${type}`);
  }

  return (
    <Query query={gqlQuery} variables={{ id }} dataComponent={Component} />
  );
};

HighlightItem.propTypes = {
  type: string.isRequired,
  id: string.isRequired,
};

export default HighlightItem;
