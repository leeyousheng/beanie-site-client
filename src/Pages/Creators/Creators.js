import React from 'react';
import {
  withStyles,
} from '@material-ui/core';
import { shape } from 'prop-types';

// Project Imports
import {
  page as pageStyle,
} from '../../styles';
import {
  Mutation,
} from '../../components';
import {
  CreateDiary,
} from '../../resources/configs';
import { getCreationForm } from './CreationForm';


const styles = theme => ({
  page: {
    ...pageStyle.responsiveContainer(theme),
    padding: theme.spacing.unit,
  },
});

const Creators = (props) => {
  const { classes } = props;
  const { gqlQuery: mutation, formInputs } = CreateDiary;

  return (
    <main className={classes.page}>
      <Mutation
        mutation={mutation}
        component={getCreationForm({
          data: formInputs,
          submitClear: true,
        })}
      />
    </main>
  );
};

Creators.propTypes = {
  classes: shape({}).isRequired,
};

export default withStyles(styles)(Creators);
