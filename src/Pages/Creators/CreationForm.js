import React from 'react';

// Project Imports
import { Form } from '../../components';

const CreationForm = (props) => {
  const { mutation, onFormSubmit, ...others } = props;
  return (
    <div>
      <Form
        {...others}
        onFormSubmit={(values) => {
          if (onFormSubmit) {
            onFormSubmit(values);
          }
          return mutation({ variables: { input: values } });
        }}
      />
    </div>
  );
};

export const getCreationForm = props => props2 => <CreationForm {...props} {...props2} />;

export default CreationForm;
