import React from 'react';
import {
  string,
  shape,
} from 'prop-types';
import {
  withStyles,
  Grid,
  Typography,
} from '@material-ui/core';

// Project Imports
import {
  Event as EventComp,
  List,
  Query,
} from '../../components';
import {
  page as pageStyle,
} from '../../styles';

const styles = theme => ({
  page: {
    ...pageStyle.responsiveContainer(theme),
    padding: theme.spacing.unit,
  },
  text: {
    textAlign: 'center',
  },
});


const GET_EVENTS = `
{
  allEvents {
    id
    title
    eventDetails {
      name
      icon
      text
    }
    description
    options {
      name
      text
      type
    }
    image {
      src
      alt
    }
  }
}
`;

const EventsListItem = (props) => {
  const { item } = props;
  return (
    <Grid item xs={12}>
      <EventComp data={item} />
    </Grid>
  );
};

EventsListItem.propTypes = {
  item: shape({}).isRequired,
};

const MidEvent = props => <List {...props} component={EventsListItem} />;

const Events = (props) => {
  const { classes } = props;

  return (
    <main className={classes.page}>
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <Typography variant="display1" className={classes.text}>
            Events
          </Typography>
        </Grid>
        <Query query={GET_EVENTS} dataComponent={MidEvent} />
      </Grid>
    </main>
  );
};

Events.propTypes = {
  classes: shape({
    page: string.isRequired,
    text: string.isRequired,
  }).isRequired,
};

export default withStyles(styles)(Events);
