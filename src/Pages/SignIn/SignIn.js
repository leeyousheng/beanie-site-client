import React from 'react';
import {
  shape,
  func,
} from 'prop-types';
import {
  withStyles,
} from '@material-ui/core';
import firebase from 'firebase/app';
import 'firebase/auth';

// Project Imports
import {
  page as pageStyle,
} from '../../styles';
import { Form } from '../../components';
import { SignIn as signInData } from '../../resources/configs/Form';

const styles = theme => ({
  page: {
    ...pageStyle.responsiveContainer(theme),
    padding: theme.spacing.unit,
  },
});

const signIn = async ({ email, password }) => {
  await firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL);
  await firebase.auth().signInWithEmailAndPassword(email, password);
  const user = firebase.auth().currentUser;
  const newToken = await user.getIdToken();
  sessionStorage.setItem('token', newToken);
  return { token: newToken };
};

const SignIn = (props) => {
  const { classes, updateState } = props;

  return (
    <main className={classes.page}>
      <Form
        data={signInData.formInputs}
        onFormSubmit={values => updateState(signIn(values))}
      />
    </main>
  );
};

SignIn.propTypes = {
  classes: shape({}).isRequired,
  updateState: func.isRequired,
};

export default withStyles(styles)(SignIn);
