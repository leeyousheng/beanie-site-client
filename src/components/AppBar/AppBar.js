import React from 'react';
import {
  shape,
  string,
  func,
} from 'prop-types';
import {
  withStyles,
  AppBar as MuiAppBar,
  Toolbar,
  Typography,
  IconButton,
} from '@material-ui/core';
import {
  Menu,
} from '@material-ui/icons';

const styles = () => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
});

const AppBar = (props) => {
  const {
    classes,
    appName,
    onMenuToggle,
  } = props;
  return (
    <div position="absolute">
      <MuiAppBar>
        <Toolbar>
          <IconButton className={classes.menuButton} color="inherit" aria-label="menu" onClick={onMenuToggle}>
            <Menu />
          </IconButton>
          <Typography className={classes.grow} color="inherit" variant="title">
            {appName}
          </Typography>
        </Toolbar>
      </MuiAppBar>
    </div>
  );
};

AppBar.propTypes = {
  classes: shape({
    menuButton: string.isRequired,
    grow: string.isRequired,
  }).isRequired,
  appName: string.isRequired,
  onMenuToggle: func.isRequired,
};

export default withStyles(styles)(AppBar);
