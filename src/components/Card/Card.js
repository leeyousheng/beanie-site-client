import React from 'react';
import {
  node,
  string,
  shape,
} from 'prop-types';
import {
  withStyles,
  Paper,
  Typography,
  Divider,
} from '@material-ui/core';

const styles = theme => ({
  root: {
    padding: theme.spacing.unit,
  },
  title: {
    textAlign: 'center',
  },
  content: {
    paddingTop: theme.spacing.unit,
  },
});

const Card = (props) => {
  const { title, children, classes } = props;
  return (
    <Paper className={classes.root}>
      {title && (
        <div>
          <Typography className={classes.title} variant="headline">
            {title.toUpperCase()}
          </Typography>
          <Divider />
        </div>
      )}
      <div className={classes.content}>
        {children}
      </div>
    </Paper>
  );
};

Card.propTypes = {
  title: string,
  children: node.isRequired,
  classes: shape({
    root: string.isRequired,
    title: string.isRequired,
    content: string.isRequired,
  }).isRequired,
};

Card.defaultProps = {
  title: undefined,
};

export default withStyles(styles)(Card);
