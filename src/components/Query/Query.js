import React from 'react';
import {
  func,
  string,
} from 'prop-types';
import { Query as ApolloQuery } from 'react-apollo';
import gql from 'graphql-tag';

// Project Imports
import DefaultLoad from './Load';
import DefaultError from './Error';

const Query = (props) => {
  const {
    query,
    dataComponent: DataComponent,
    loadComponent: LoadComponent,
    errorComponent: ErrorComponent,
    ...others
  } = props;
  return (
    <ApolloQuery query={gql`${query}`} {...others}>
      {({ loading, error, data }) => {
        if (loading) return <LoadComponent data={loading} />;
        if (error) return <ErrorComponent data={error} />;

        const compData = Object.keys(data).map(key => data[key])[0];
        return (
          <DataComponent data={compData} />
        );
      }}
    </ApolloQuery>
  );
};

Query.propTypes = {
  query: string.isRequired,
  dataComponent: func.isRequired,
  loadComponent: func,
  errorComponent: func,
};

Query.defaultProps = {
  loadComponent: DefaultLoad,
  errorComponent: DefaultError,
};

export default Query;
