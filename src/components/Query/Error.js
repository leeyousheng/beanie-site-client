import React from 'react';
import {
  shape,
  string,
} from 'prop-types';

const ErrorComp = (props) => {
  const { data } = props;
  const { message } = data;
  return (
    <p>
      Opps something went wrong. Error: ${message}
    </p>
  );
};

ErrorComp.propTypes = {
  data: shape({
    message: string.isRequired,
  }).isRequired,
};

export default ErrorComp;
