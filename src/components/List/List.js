import React from 'react';
import {
  shape,
  func,
  arrayOf,
} from 'prop-types';

const List = (props) => {
  const {
    data,
    component: Component,
  } = props;
  return (
    data.map(item => (
      <Component key={item.id} item={item} />
    ))
  );
};

List.propTypes = {
  data: arrayOf(shape({}).isRequired).isRequired,
  component: func.isRequired,
};

export default List;
