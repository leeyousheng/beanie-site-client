import React from 'react';
import { string, shape, arrayOf } from 'prop-types';
import {
  withStyles,
  Grid,
  Card,
  CardHeader,
  CardContent,
} from '@material-ui/core';

// Project Imports
import Tag from '../Tag/Tag';
import { colour } from '../../styles';
import Text from '../Text';
import { DateUtil } from '../../utils';
import Avatar from '../Avatar';

const styles = () => ({
  card: {
    ...colour.diary,
  },
});

const renderTags = (tags) => {
  if (tags) {
    return (
      Object.keys(tags).map(key => (
        <Grid key={key} item>
          <Tag tag={tags[key]} />
        </Grid>
      ))
    );
  }
  return null;
};

const Diary = (props) => {
  const { data, classes } = props;
  const {
    title,
    entryDate,
    text,
    tags,
    author,
  } = data;
  const { pic, displayname } = { pic: null, displayname: 'anon', ...author };

  return (
    <Card classes={{ root: classes.card }} raised>
      <CardHeader
        avatar={<Avatar src={pic} username={author ? author.displayname : undefined} iconName="PermIdentity" />}
        title={title}
        subheader={`${displayname} on ${DateUtil.parseToDateString(entryDate)}`}
      />
      <CardContent>
        <Grid container spacing={8}>
          {renderTags(tags)}
          <Grid item xs={12}>
            <Text text={text} />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

Diary.propTypes = {
  classes: shape({
    card: string.isRequired,
  }).isRequired,
  data: shape({
    title: string.isRequired,
    text: string.isRequired,
    tags: arrayOf(shape({})).isRequired,
  }).isRequired,
};

Diary.defaultProps = {
  tags: {},
};

export default withStyles(styles)(Diary);
