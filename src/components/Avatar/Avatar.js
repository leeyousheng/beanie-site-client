import React from 'react';
import {
  string,
} from 'prop-types';
import {
  Avatar as MuiAvatar,
} from '@material-ui/core';

// Project Imports
import Icon from '../Icon';

const renderChildren = ({ iconName, username, children }) => {
  if (username) {
    const names = username.toUpperCase().split(' ');
    return `${names[0][0]}${names[1] ? `${names[1][0]}` : ''}`;
  }
  if (iconName) return <Icon name={iconName} />;
  return children;
};

const Avatar = (props) => {
  const {
    iconName,
    username,
    children,
    ...others
  } = props;
  return (
    <MuiAvatar {...others}>
      {renderChildren({ iconName, username, children })}
    </MuiAvatar>
  );
};

Avatar.propTypes = {
  iconName: string,
};

Avatar.defaultProps = {
  iconName: undefined,
};

export default Avatar;
