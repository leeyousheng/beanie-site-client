import React from 'react';
import {
  shape,
  string,
  func,
  bool,
  arrayOf,
} from 'prop-types';
import {
  Button,
} from '@material-ui/core';

// Project Imports
import FormFields from './FormFields';

const processInputNodes = (nodes) => {
  const inputs = {};
  Object.keys(nodes).map(key => inputs[key] = nodes[key].value);
  return inputs;
};

const clearForm = (nodes) => {
  // eslint-disable-next-line no-param-reassign
  Object.keys(nodes).forEach(key => nodes[key].value = '');
};

const Form = (props) => {
  const {
    classes,
    submitButtonText,
    data,
    onFormSubmit,
    submitClear,
  } = props;
  const inputNodes = {};

  const handleOnFormSubmit = (evt) => {
    evt.preventDefault();
    onFormSubmit(processInputNodes(inputNodes));
    if (submitClear) {
      clearForm(inputNodes);
    }
  };

  return (
    <form className={classes.form} onSubmit={handleOnFormSubmit}>
      <FormFields className={classes.fields} data={data} inputNodes={inputNodes} />
      <Button
        type="submit"
        fullWidth
        variant="raised"
        color="primary"
        className={classes.submit}
      >
        {submitButtonText}
      </Button>
    </form>
  );
};

Form.propTypes = {
  classes: shape({
    form: string,
    fields: string,
    submit: string,
  }),
  submitButtonText: string,
  data: arrayOf(shape({})).isRequired,
  onFormSubmit: func.isRequired,
  submitClear: bool,
};

Form.defaultProps = {
  submitButtonText: 'Submit',
  classes: {},
  submitClear: false,
};

export default Form;
