import React from 'react';
import {
  shape, arrayOf,
} from 'prop-types';
import {
  FormControl,
  InputLabel,
  Input,
} from '@material-ui/core';

const FormFields = (props) => {
  const {
    data,
    inputNodes,
    formControl,
    inputLabel,
    input,
  } = props;

  return Object.keys(data).map((key) => {
    const {
      name,
      label,
      type,
      flags,
      defaultValue,
    } = data[key];

    return (
      <FormControl key={key} margin="normal" required fullWidth {...formControl}>
        <InputLabel htmlFor={name} {...inputLabel}>
          {label}
        </InputLabel>
        <Input
          id={name}
          name={name}
          type={type}
          inputRef={(node) => {
            if (!node) return undefined;

            if (defaultValue) {
              // eslint-disable-next-line no-param-reassign
              node.value = defaultValue;
            }
            return inputNodes[name] = node;
          }}
          {...input}
          {...flags}
        />
      </FormControl>
    );
  });
};

FormFields.propTypes = {
  data: arrayOf(shape({})).isRequired,
  inputNodes: shape({}).isRequired,
  formControl: shape({}),
  inputLabel: shape({}),
  input: shape({}),
};

export default FormFields;
