import React from 'react';
import {
  shape,
  string,
} from 'prop-types';
import {
  withStyles,
  Typography,
} from '@material-ui/core';
import classNames from 'classnames';

// Project Imports
import { colour } from '../../styles';

const styles = () => ({
  baseTag: {
    borderRadius: 4,
    padding: '4px 8px',
  },
  ...colour.tag,
});

const getClassFromType = (type, classes) => (
  classes[type] || classes.default
);

const Tag = (props) => {
  const { classes, tag } = props;
  const { text, type } = tag;

  const allClasses = [classes.baseTag];
  allClasses.push(getClassFromType(type, classes));

  return (
    <Typography className={classNames(allClasses)} variant="caption">
      {text}
    </Typography>
  );
};

Tag.propTypes = {
  classes: shape({
    baseTag: string.isRequired,
    default: string.isRequired,
    info: string.isRequired,
    event: string.isRequired,
  }).isRequired,
  tag: shape({
    text: string.isRequired,
    type: string.isRequired,
  }).isRequired,
};

export default withStyles(styles)(Tag);
