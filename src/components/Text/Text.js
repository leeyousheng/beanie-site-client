import React from 'react';
import {
  shape,
  string,
} from 'prop-types';
import classNames from 'classnames';
import { Typography } from '@material-ui/core';

const renderText = (text, delimiter, variant, classes) => (
  text.split(delimiter).map((textpart, i) => (
    <Typography
      className={classes.textpart}
      // eslint-disable-next-line react/no-array-index-key
      key={i}
      variant={variant}
    >
      {textpart}
    </Typography>
  ))
);

const Text = (props) => {
  const {
    classes,
    className: classNameProp,
    text,
    delimiter,
    variant,
  } = props;

  const allClassnames = classNames(classNameProp, classes.text);
  return (
    <div className={allClassnames}>
      {renderText(text, delimiter, variant, classes)}
    </div>
  );
};

Text.propTypes = {
  classes: shape({
    // text: For the entire text component
    text: string,
    // textpart: For the each individual text part
    textpart: string,
  }),
  className: string,
  text: string.isRequired,
  delimiter: string,
  variant: string,
};

Text.defaultProps = {
  classes: {},
  className: '',
  delimiter: '\n',
  variant: 'body1',
};

export default Text;
