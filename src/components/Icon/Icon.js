import React from 'react';
import {
  string,
} from 'prop-types';

import * as Icons from '@material-ui/icons';

const Icon = (props) => {
  const { name, className } = props;
  const foundKeys = Object.keys(Icons)
    .filter(key => key.toLowerCase() === name.toLowerCase());

  if (foundKeys.length < 1) {
    return <p className="className">{name}</p>;
  }

  const Component = Icons[foundKeys[0]];
  return <Component className={className} />;
};

Icon.propTypes = {
  className: string,
  name: string.isRequired,
};

Icon.defaultProps = {
  className: undefined,
};

export default Icon;
