import React from 'react';
import { Link } from 'react-router-dom';
import {
  string,
  func,
  shape,
  bool,
} from 'prop-types';
import {
  Drawer as MuiDrawer,
  Divider,
  ListItem,
  ListItemIcon,
  ListItemText,
  withStyles,
} from '@material-ui/core';

// Project Imports

const styles = () => ({
  list: {
    width: 250,
  },
});

const renderItems = (items = {}) => (
  Object.keys(items).map((key) => {
    const { icon, label, url } = items[key];
    return (
      <div key={key}>
        <ListItem button component={Link} to={url || '/'}>
          {icon ? (
            <ListItemIcon>
              {icon}
            </ListItemIcon>
          ) : <div />}
          <ListItemText>
            {label}
          </ListItemText>
        </ListItem>
      </div>
    );
  })
);

const renderMenuItems = menuItems => (
  Object.keys(menuItems).map(key => (
    <div key={key}>
      {renderItems(menuItems[key])}
      <Divider />
    </div>
  ))
);

const Drawer = (props) => {
  const {
    classes,
    isMenuOpen,
    onMenuClose,
    menuItems,
  } = props;

  return (
    <div>
      <MuiDrawer
        open={isMenuOpen}
        onClose={onMenuClose}
      >
        <div
          tabIndex={0}
          role="button"
          onClick={onMenuClose}
          onKeyDown={onMenuClose}
          className={classes.list}
        >
          {renderMenuItems(menuItems)}
        </div>
      </MuiDrawer>
    </div>
  );
};

Drawer.propTypes = {
  classes: shape({
    list: string.isRequired,
  }).isRequired,
  isMenuOpen: bool.isRequired,
  onMenuClose: func.isRequired,
  menuItems: shape({}).isRequired,
};

export default withStyles(styles)(Drawer);
