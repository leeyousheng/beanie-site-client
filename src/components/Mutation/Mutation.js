import React from 'react';
import {
  string,
  func,
} from 'prop-types';
import {
  Mutation as ApolloMutation,
} from 'react-apollo';
import gql from 'graphql-tag';

const Mutation = (props) => {
  const {
    mutation,
    component: Component,
  } = props;

  return (
    <ApolloMutation mutation={gql`${mutation}`}>
      {(mutationFunc, result) => <Component mutation={mutationFunc} result={result} />}
    </ApolloMutation>
  );
};

Mutation.propTypes = {
  mutation: string.isRequired,
  component: func.isRequired,
};

export default Mutation;
