import React from 'react';
import {
  withStyles,
  Grid,
  Button,
  Card,
  CardContent,
  CardHeader,
  CardActions,
  CardMedia,
  ListItem,
  ListItemText,
  ListItemIcon,
} from '@material-ui/core';
import {
  shape,
  string,
  bool,
  arrayOf,
} from 'prop-types';

// Project Import
import Avatar from '../Avatar';
import Icon from '../Icon';
import { colour } from '../../styles';

const styles = theme => ({
  card: {
    ...colour.event,
  },
  eventDetails: {
    padding: theme.spacing.unit,
  },
  icon: {
    margin: 0,
  },
  media: {
    height: '100%',
    minHeight: 200,
  },
  content: {
    padding: theme.spacing.unit,
  },
  flexgrow: {
    flexGrow: 1,
  },
});

const renderImage = (image, classes, small) => {
  if (image) {
    return (
      <Grid item md={small ? 12 : 4} xs={12}>
        <CardMedia
          className={classes.media}
          image={image.src}
          title={image.alt}
        />
      </Grid>
    );
  }
  return undefined;
};

const renderEventDetails = (details, classes) => (
  Object.keys(details).map((key) => {
    const { icon, text, onClick } = details[key];

    return (
      <ListItem key={key} className={classes.eventDetails} button onClick={onClick}>
        <ListItemIcon className={classes.icon}>
          <Icon name={icon} />
        </ListItemIcon>
        <ListItemText primary={text} />
      </ListItem>
    );
  })
);

const renderOptions = options => (
  Object.keys(options).map((key) => {
    const { text, type, onClick } = options[key];
    return (
      <Button key={key} variant="contained" color={type} onClick={onClick}>
        {text}
      </Button>
    );
  })
);

const EventCard = (props) => {
  const {
    classes,
    data,
    flat,
    small,
  } = props;
  const {
    title,
    eventDetails,
    description,
    options,
    image,
    sub,
  } = data;

  return (
    <Card classes={{ root: classes.card }} raised={!flat}>
      <CardHeader avatar={<Avatar iconName="event" />} title={title} subheader={sub} />
      <div className={classes.content}>
        <Grid container spacing={8}>
          {renderImage(image, classes, small)}
          <Grid item md={small ? 12 : 8} xs={12}>
            <CardContent>
              <Grid container spacing={8}>
                <Grid item xs={12}>
                  {description}
                </Grid>
                <Grid item xs={12}>
                  {renderEventDetails(eventDetails, classes)}
                </Grid>
              </Grid>
            </CardContent>
            <Grid item xs={12}>
              <CardActions>
                {renderOptions(options, classes)}
              </CardActions>
            </Grid>
          </Grid>
        </Grid>
      </div>
    </Card>
  );
};

EventCard.propTypes = {
  classes: shape({
    card: string.isRequired,
    content: string.isRequired,
    media: string.isRequired,
    eventDetails: string.isRequired,
    icon: string.isRequired,
  }).isRequired,
  data: shape({
    title: string.isRequired,
    eventDetails: arrayOf(shape({})).isRequired,
    description: string.isRequired,
    options: arrayOf(shape({})).isRequired,
    image: shape({
      src: string,
      alt: string,
    }),
  }).isRequired,
  flat: bool,
  small: bool,
};

EventCard.defaultProps = {
  flat: false,
  small: false,
};

export default withStyles(styles)(EventCard);
