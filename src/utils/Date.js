const DAYS = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

// Format to YYYY-MM-DD
const getInputDate = (date = new Date()) => date.toISOString().slice(0, 10);

const parseToDateString = (date) => {
  if (date) {
    const cur = new Date(date);
    return `[${DAYS[cur.getDay()]}] ${cur.getDate()}/${cur.getMonth() + 1}/${cur.getFullYear()}`;
  }
  return null;
};

export default {
  getInputDate,
  parseToDateString,
  DAYS,
};
