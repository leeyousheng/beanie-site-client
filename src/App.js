import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { shape, string } from 'prop-types';
import { withStyles } from '@material-ui/core';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

// Project Imports
import {
  AppBar,
  Drawer,
} from './components';
import {
  LandingPage,
  Page404,
  Diary,
  Events,
  Creators,
  SignIn,
} from './Pages';
import {
  MenuConfig,
} from './resources/configs';

const styles = theme => ({
  toolbar: theme.mixins.toolbar,
});

const uriHead = process.env.NODE_ENV === 'production' ? '' : 'http://localhost:4000';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMenuOpen: false,
      token: sessionStorage.getItem('token') || '',
    };
  }

  render() {
    const { isMenuOpen } = this.state;
    const { classes } = this.props;
    const { token } = this.state;

    const SignInComp = props => (
      <SignIn
        updateState={async newState => this.setState(await newState)}
        {...props}
      />
    );
    return (
      <ApolloProvider client={new ApolloClient({ uri: `${uriHead}/graphql`, headers: { authorization: token } })}>
        <AppBar
          appName="Beanie"
          onMenuToggle={() => (
            this.setState({
              isMenuOpen: !isMenuOpen,
            })
          )}
        />
        <Router>
          <div>
            <Drawer
              isMenuOpen={isMenuOpen}
              menuItems={MenuConfig.items}
              onMenuClose={() => (
                this.setState({
                  isMenuOpen: false,
                })
              )}
            />
            <div className={classes.toolbar} />
            <Switch>
              <Route exact path="/" component={LandingPage} />
              <Route path="/diary" component={Diary} />
              <Route path="/events" component={Events} />
              <Route path="/signin" component={SignInComp} />
              <Route path="/creators" component={Creators} />
              <Route component={Page404} />
            </Switch>
          </div>
        </Router>
      </ApolloProvider>
    );
  }
}

App.propTypes = {
  classes: shape({
    toolbar: string.isRequired,
  }).isRequired,
};

export default withStyles(styles)(App);
